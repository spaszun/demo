# Recruitment task

Task for allegrotech


## Running

You can run the app by typing:

```
./mvnw spring-boot:run
```

## invoking

Call the service with

```
curl --header "Accept: application/vnd.spaszun-v1.0+json" --header "Accept-Language: dk" http://localhost:8080/repositories/allegro/hermes
```

## Running tests

Invoke unit and e2e tests with command

```
./mvnw verify
```

## Hystrix dashboard

Hystrix dashboard is configured on http://localhost:8080/hystrix. Type http://localhost:8080/hystrix.stream in url to see it in action