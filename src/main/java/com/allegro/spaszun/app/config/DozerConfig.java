package com.allegro.spaszun.app.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Stanisław on 2016-06-11.
 */
@Configuration
public class DozerConfig {

    @Bean
    public DozerBeanMapper getMapper() {
        return new DozerBeanMapper();
    }

}
