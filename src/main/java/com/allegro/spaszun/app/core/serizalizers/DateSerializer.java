package com.allegro.spaszun.app.core.serizalizers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateSerializer extends JsonSerializer<Date> {


    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2)
            throws IOException, JsonProcessingException {
        Locale locale = LocaleContextHolder.getLocale();
        DateFormat df = new SimpleDateFormat(DateConstants.FORMAT, locale);
        if (value == null) {
            gen.writeNull();
        } else {
            gen.writeString(df.format(value.getTime()));
        }
    }
}