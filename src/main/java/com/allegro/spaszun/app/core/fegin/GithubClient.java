package com.allegro.spaszun.app.core.fegin;


import com.allegro.spaszun.app.core.fegin.vo.GithubRepository;
import com.allegro.spaszun.config.FeignConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="github", url = "https://api.github.com", configuration = FeignConfig.class)
public interface GithubClient {

    @RequestMapping(value = "/repos/{owner}/{repositoryName}", method = RequestMethod.GET,
            produces = "application/vnd.github.v3+json")
    GithubRepository getRepository(@PathVariable("owner") String owner, @PathVariable("repositoryName") String repositoryName);
}
