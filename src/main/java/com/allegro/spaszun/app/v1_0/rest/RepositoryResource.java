package com.allegro.spaszun.app.v1_0.rest;

import com.allegro.spaszun.app.Versions;
import com.allegro.spaszun.app.core.fegin.GithubClient;
import com.allegro.spaszun.app.core.fegin.vo.GithubRepository;
import com.allegro.spaszun.app.v1_0.vo.RepositoryVO;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "repositories", produces = Versions.V1_0)
public class RepositoryResource {

    @Autowired
    GithubClient gitHubClient;

    @Autowired
    DozerBeanMapper dozerBeanMapper;

    @RequestMapping(value = "/{owner}/{repositoryName}", method = RequestMethod.GET)
    public RepositoryVO getRepository(@PathVariable String owner, @PathVariable String repositoryName) {
        GithubRepository githubRepository = gitHubClient.getRepository(owner, repositoryName);
        RepositoryVO repo = dozerBeanMapper.map(githubRepository, RepositoryVO.class);
        return repo;
    }

}
