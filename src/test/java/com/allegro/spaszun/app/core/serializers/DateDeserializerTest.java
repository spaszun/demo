package com.allegro.spaszun.app.core.serializers;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.allegro.spaszun.app.core.serizalizers.DateSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.i18n.LocaleContextHolder;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.*;
@RunWith(PowerMockRunner.class)
@PrepareForTest( { LocaleContextHolder.class, })
public class DateDeserializerTest {

    @Test
    public void testDeserializing() throws Exception {
        //given
        mockStatic(LocaleContextHolder.class);
        expect(LocaleContextHolder.getLocale()).andReturn(Locale.FRANCE);
        replay(LocaleContextHolder.class);
        Writer jsonWriter = new StringWriter();
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(jsonWriter);
        SerializerProvider serializerProvider = new ObjectMapper().getSerializerProvider();
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        instance.set(2000, Calendar.MARCH, 3, 12, 12, 0);
        Date dateFromOld = instance.getTime();

        //when
        new DateSerializer().serialize(dateFromOld, jsonGenerator, serializerProvider);
        jsonGenerator.flush();
        String parsedDate = jsonWriter.toString();

        //then
        assertEquals("\"ven., 3 mars 2000 13:12:00 +0100\"", parsedDate);
        verify(LocaleContextHolder.class);
    }


}