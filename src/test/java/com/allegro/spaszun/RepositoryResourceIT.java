package com.allegro.spaszun;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.allegro.spaszun.app.Versions;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebIntegrationTest
public class RepositoryResourceIT {

    // This will hold the port number the server was started on
    @Value("${local.server.port}")
    int port;

    final RestTemplate template = new RestTemplate();

    @Test
    public void testGetMessage() throws IOException {

        //given
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accepts", Versions.V1_0);
        HttpEntity entity = new HttpEntity(headers);
        final String url = "http://localhost:{port}/repositories/{owner}/{repo}";
        final Object[] params = {port, "allegro", "hermes"};

        //when
        HttpEntity<String> response = template.exchange(url, HttpMethod.GET, entity, String.class, params);
        String json = response.getBody();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});

        //then
        Assert.assertNotNull(map);
        Assert.assertNotNull(map.get("fullName"));
    }
}

